package server;

import com.sun.syndication.io.FeedException;
import server.structure.ServerPart;

import java.io.IOException;


public class MainServer {
    public static void main(String[] args) throws IOException, FeedException, ClassNotFoundException {
        ServerPart serverPart = new ServerPart();

        Thread thread = new Thread(serverPart);
        thread.run();
        try {
            thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping Server");
    }
}
