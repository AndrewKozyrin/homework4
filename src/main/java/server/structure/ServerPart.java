package server.structure;


import com.sun.syndication.io.FeedException;
import resources.Constant;
import resources.zipStructure.ZipUnpacking;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerPart implements Runnable {

    public void run() {
        getServerSocket();
    }

    protected void getServerSocket() {
        try {
            ServerSocket serverSocket = new ServerSocket(Constant.getPORT());
            ServerSocket serverSocketXML = new ServerSocket(Constant.getPORTXML());
            System.out.println("Server started: " + serverSocket);
            System.out.println("Server started: " + serverSocketXML);
            System.out.println("\n" + "You need start client, to see news!");
            getAcceptFile(serverSocket);
            getAcceptXML(serverSocketXML);
            MainMenu.command();
        } catch (FeedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void getAcceptFile(ServerSocket serverSocket) throws IOException, FeedException, ClassNotFoundException {
        while (true) {
            Socket clientSocket = serverSocket.accept();
            File file = new File(Constant.getZIPSERVERFILE());
            getFileOnClient(clientSocket, file);
            ZipUnpacking.unPack(Constant.getSERVERFILENAME(), Constant.getZIPSERVERFILE());
            System.out.println("File Ready");
            return;
        }
    }

    protected void getAcceptXML(ServerSocket serverSocket) throws IOException, FeedException, ClassNotFoundException {
        while (true) {
            Socket clientSocket = serverSocket.accept();
            File fileXML = new File(Constant.getZIPSERVERXML());
            getFileOnClient(clientSocket, fileXML);
            ZipUnpacking.unPack(Constant.getSERVERXMLNAME(), Constant.getZIPSERVERXML());
            System.out.println("XML Ready");
            return;
        }
    }

    private static void getFileOnClient(Socket socket, File file) {
        try (InputStream is = socket.getInputStream();
             OutputStream os = new FileOutputStream(file)) {
            byte[] buffer = new byte[1024];
            for (int count = is.read(buffer); count != -1; count = is.read(buffer)) {
                os.write(buffer, 0, count);
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}

