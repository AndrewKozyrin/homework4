package server.structure;

import client.structure.ClientPart;
import com.sun.syndication.io.FeedException;
import server.structure.fileParser.FileRssParser;
import server.structure.xmlParser.XmlRssParser;

import java.io.IOException;
import java.util.Scanner;

public class MainMenu {
    public static void command() throws ClassNotFoundException, IOException, FeedException {
        System.out.println("\n" + "Check number, to see news!");

        System.out.println("1: TLT News");
        System.out.println("2: CNN World News");
        System.out.println("0: Close Sever");
        System.out.println(">");
        Scanner scanner = new Scanner(System.in);
        try {


            int choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1:
                    new FileRssParser().parser();
                    break;
                case 2:
                    new XmlRssParser().parser();
                    break;
                case 0:
                    System.out.println("Server will stop, through 10 second");
                    break;
                default:
                    System.out.println("Unexpected command!");
                    MainMenu.command();
            }
        } catch (NumberFormatException e) {
            System.out.println("Number is not correct, repeat your entry");
            MainMenu.command();
        }
    }

    public static void getBack() throws FeedException, IOException, ClassNotFoundException {
        System.out.println("1: TLT News");
        System.out.println("2: CNN World News");
        System.out.println("9: Updated News");
        System.out.println("0: Close Sever");
        System.out.println(">");
        Scanner scanner = new Scanner(System.in);
        try {
            int choice = Integer.parseInt(scanner.nextLine());

            switch (choice) {
                case 1:
                    new FileRssParser().parser();
                    break;
                case 2:
                    new XmlRssParser().parser();
                    break;
                case 0:
                    System.out.println("Server will stop, through 10 second");
                    break;
                case 9:
                    new ClientPart().run();
                    new ClientPart().connect();
                    MainMenu.command();
                    break;
                default:
                    System.out.println("Unexpected command!");
                    MainMenu.getBack();
            }
        } catch (NumberFormatException e) {
            System.out.println("Number is not correct, repeat your entry");
            MainMenu.getBack();
        }
    }
}

