package server.structure.fileParser;

import com.sun.syndication.io.FeedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class FileReader {
    public List readFile() throws IOException, FeedException, ClassNotFoundException {
        List clubs = null;
        FileInputStream fis = new FileInputStream("serverNews.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        clubs = (List) ois.readObject();
        ois.close();
        System.out.println("File Read");
        return clubs;
    }
}
