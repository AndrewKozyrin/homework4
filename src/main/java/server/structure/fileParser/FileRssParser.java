package server.structure.fileParser;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.io.FeedException;
import server.structure.MainMenu;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class FileRssParser {


    public void parser() throws ClassNotFoundException, IOException, FeedException {
        List parser = new FileReader().readFile();
        Iterator<SyndEntry> itEntries = parser.iterator();

        while (itEntries.hasNext()){
            SyndEntry entry = itEntries.next();
            System.out.println("About feed:");
            System.out.println("Author: " + entry.getAuthor());
            System.out.println("Authors:");
            if (entry.getAuthors() != null) {
                for (Object author : entry.getAuthors()) {
                    System.out.println(((SyndPerson) author).getName());
                    System.out.println(((SyndPerson) author).getEmail());
                    System.out.println(((SyndPerson) author).getUri());
                    System.out.println();
                }
            }
            System.out.println("Title: " + entry.getTitle());
            System.out.println("Description: " + entry.getDescription());
            System.out.println("Date" + entry.getPublishedDate());
            System.out.println();
        }
        MainMenu.getBack();
    }
}
