package server.structure.xmlParser;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import server.structure.MainMenu;

import java.io.File;
import java.io.IOException;

public class XmlRssParser {

    public void parser() throws FeedException, IOException, ClassNotFoundException {
        printRSSContent(xmlFeed("serverNewsXms.xml"));
    }

    public SyndFeed xmlFeed(String xml) throws ClassNotFoundException, IOException, FeedException{
        return new SyndFeedInput().build(new XmlReader(new File(xml)));
    }

    public void printRSSContent(SyndFeed feed) throws ClassNotFoundException, IOException, FeedException {
        System.out.println("About feed:");
        System.out.println("Author: " + feed.getAuthor());
        System.out.println("Authors:");
        System.out.println("Title: " + feed.getTitle());
        System.out.println("Title Ex: " + feed.getTitleEx());
        System.out.println("Description: " + feed.getDescription());
        System.out.println("Description Ex: " + feed.getDescriptionEx());
        System.out.println("Date" + feed.getPublishedDate());
        System.out.println("Type: " + feed.getFeedType());
        System.out.println("Encoding: " + feed.getEncoding());
        System.out.println("(C) " + feed.getCopyright());
        System.out.println();
        for (Object object : feed.getEntries()) {
            SyndEntry entry = (SyndEntry) object;
            System.out.println(entry.getTitle() + " - " + entry.getAuthor());
            System.out.println(entry.getLink());
            for (Object contobj : entry.getContents()) {
                SyndContent content = (SyndContent) contobj;
                System.out.println(content.getType());
                System.out.println(content.getValue());
            }
            SyndContent content = entry.getDescription();
            if (content != null)
                System.out.println(content.getValue());
            System.out.println(entry.getPublishedDate());
            System.out.println();
        }
        MainMenu.getBack();
    }
}
