package client.structure;

import resources.Constant;
import java.io.*;
import java.net.Socket;

public class Connection {

    static File getClientZipFile() {
        return new File(Constant.getZIPFILENAME());
    }
    static File getClientZipXML(){
        return new File(Constant.getZIPFILENAMEXML());
    }

    public static void getConnection() {
        try {
            Socket client = new Socket(resources.Constant.getHOST(), resources.Constant.getPORT());
            Socket socket = new Socket(resources.Constant.getHOSTXML(), resources.Constant.getPORTXML());

            File clientZip = getClientZipFile();
            Connection.putDataOnServer(client, clientZip);

            File clientZipXML = getClientZipXML();
            Connection.putDataOnServer(socket, clientZipXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void putDataOnServer(Socket socket, File file) {
        try (OutputStream os = socket.getOutputStream();
             InputStream is = new FileInputStream(file)) {

            byte[] buffer = new byte[1024];

            for (int count = -1; (count = is.read(buffer)) != -1; ) {
                os.write(buffer, 0, count);
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}
