package client.structure.xmlStructure;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import resources.Constant;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;


public class XmlReader {

    public static void xmlReader() throws IOException, ParserConfigurationException, SAXException, TransformerException {
        URL url = new URL("http://rss.cnn.com/rss/edition_world.rss");
        URLConnection conn = url.openConnection();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(conn.getInputStream());

        TransformerFactory factorys = TransformerFactory.newInstance();
        Transformer xform = factorys.newTransformer();

        xform.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(Constant.getFILENAMEXML())));
    }
}
