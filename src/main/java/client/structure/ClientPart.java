package client.structure;

import client.structure.fileStructure.WriteListToFile;
import client.structure.xmlStructure.XmlReader;
import com.sun.syndication.io.FeedException;
import org.xml.sax.SAXException;
import resources.Constant;
import resources.zipStructure.ZipCreator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class ClientPart {

    public void run() {
        try {
            WriteListToFile.writeToFile();
            XmlReader.xmlReader();
            ZipCreator.createZip(Constant.getZIPFILENAME(), Constant.getFILENAME());
            ZipCreator.createZip(Constant.getZIPFILENAMEXML(), Constant.getFILENAMEXML());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FeedException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
    public void connect(){
        Connection.getConnection();
        System.out.println("Connected, files send");
    }
}

