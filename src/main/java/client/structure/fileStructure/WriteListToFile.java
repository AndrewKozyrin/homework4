package client.structure.fileStructure;

import client.structure.fileStructure.RSSFeedToList;
import com.sun.syndication.io.FeedException;
import resources.Constant;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class WriteListToFile {

    public static void writeToFile() throws IOException, FeedException {

        List listToWrite = new RSSFeedToList().feed();

        try {
            FileOutputStream fos = new FileOutputStream(Constant.getFILENAME());
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(listToWrite);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            System.out.println("File Wrote");
        }
    }
}
