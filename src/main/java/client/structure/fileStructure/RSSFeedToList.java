package client.structure.fileStructure;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class RSSFeedToList {

    public List feed() throws IOException, FeedException {

        List<SyndEntry> entries = null;
        URL url = new URL("http://tlt.ru/rss/index.rss");

        HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = null;
        feed = input.build(new XmlReader(httpcon));
        entries = feed.getEntries();
        return entries;
    }
}
