package client;


import client.structure.ClientPart;

public class MainClient {
    public static void main(String[] args) throws InterruptedException {
        ClientPart clientPart = new ClientPart();

        clientPart.run();
        synchronized (clientPart) {
            clientPart.wait(3 * 1000);
        }
        clientPart.connect();
    }
}
