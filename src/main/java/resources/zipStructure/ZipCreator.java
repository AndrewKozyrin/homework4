package resources.zipStructure;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCreator {

    public static void createZip(String ZIPFILENAME, String FILENAME) {

        File zipFile = new File(ZIPFILENAME);

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            addToZip(zos, FILENAME);

            System.out.println("File added to ZIP! Path: ("
                    + zipFile.getAbsolutePath() + ")");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addToZip(ZipOutputStream zipToWrite, String listToWrite) throws IOException {

        File pathToXML = new File(listToWrite);

        FileInputStream writeStream = new FileInputStream(pathToXML);
        ZipEntry zipEntry = new ZipEntry(listToWrite);
        zipToWrite.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;

        while ((length = writeStream.read(bytes)) >= 0) {
            zipToWrite.write(bytes, 0, length);
        }

        zipToWrite.close();
        writeStream.close();
    }
}

