package resources.zipStructure;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUnpacking {

    public static void unPack(String fileName, String pathToZip) {
        File pathToServerZip = new File(pathToZip);
        byte[] buffer = new byte[1024];

        try (ZipInputStream zipinputstream = new ZipInputStream(
                new FileInputStream(pathToServerZip))) {
            ZipEntry zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) {
                int n;
                try (FileOutputStream fileoutputstream = new FileOutputStream(fileName)) {
                    while ((n = zipinputstream.read(buffer, 0, 1024)) > -1) {
                        fileoutputstream.write(buffer, 0, n);
                    }
                }
                zipentry = zipinputstream.getNextEntry();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
