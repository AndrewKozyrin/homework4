package resources;


public class Constant {
    private static final String HOST = "127.0.0.1";
    private static final String HOSTXML = "127.0.0.2";
    private static final int PORT = 3575;
    private static final int PORTXML = 3576;
    private static final String ZIPSERVERFILE = "ServerNews.zip";
    private static final String ZIPSERVERXML = "ServerNewsXML.zip";
    private static final String ZIPFILENAME = "clientNews.zip";
    private static final String FILENAME = "clientNews.txt";
    private static final String ZIPFILENAMEXML = "clientNewsXml.zip";
    private static final String FILENAMEXML = "clientNews.xml";
    private static final String SERVERFILENAME = "serverNews.txt";
    private static final String SERVERXMLNAME = "serverNewsXms.xml";

    public static String getSERVERFILENAME() {
        return SERVERFILENAME;
    }

    public static String getSERVERXMLNAME() {
        return SERVERXMLNAME;
    }

    public static String getZIPFILENAME() {
        return ZIPFILENAME;
    }

    public static String getFILENAME() {
        return FILENAME;
    }

    public static String getZIPFILENAMEXML() {
        return ZIPFILENAMEXML;
    }

    public static String getFILENAMEXML() {
        return FILENAMEXML;
    }

    public static String getHOST() {
        return HOST;
    }

    public static int getPORT() {
        return PORT;
    }

    public static String getHOSTXML() {
        return HOSTXML;
    }

    public static int getPORTXML() {
        return PORTXML;
    }

    public static String getZIPSERVERFILE() {
        return ZIPSERVERFILE;
    }

    public static String getZIPSERVERXML() {
        return ZIPSERVERXML;
    }
}
